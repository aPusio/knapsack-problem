#include <iostream>
using namespace std;
namespace something {

	int max(const int a, const int b) {
		if (a > b) return a;
		return b;
	}

	void pakowanie_plecaka(int *W, int *C, const int pojemnosc, const int N) {

		//----------stworzenie tablicy pomocniczej K ----------
		//w tej tablicy sa wpisywane obliczone maxymalne udzwigi
		int **K = new int*[N];
		for (int i = 0; i < N; ++i)
			K[i] = new int[pojemnosc]();
		//-----------------------------------------------------
		for (int i = 1; i < N; ++i)					// i - aktualny przedmiot
		{
			for (int j = 1; j < pojemnosc; ++j)		// j - aktualna maxymalna waga
			{
				//	jesli maxymalna waga (kolumna tabeli) jest mniejsza niz waga rozpatrywanego przedmiotu
				//	czyli jesli rozpatrywany przedmiot nie miesci sie
				if (j < W[i])
					//przepisanie ostatniej wartosci
					K[i][j] = K[i - 1][j];
				else
					//K[i - 1][j - W[i]] - sprawdz wartosc w tabeli o jeden wyzej i w lewo o wage rozpatrywanego elementu
					// jest to waga bez ostatnio dodanego przedmiotu
					// po dodaniu do tego CI wyjdzie waga po dodaniu aktualnie rozpatrywanego punktu
					// K[i - 1][j] - to jest ostatnio wyliczona wartosc dla danego przedmiotu
					//
					// wybierasz maxymalna wartosc sposrod tych dwoch

					K[i][j] = max(K[i - 1][j - W[i]] + C[i], K[i - 1][j]);
			}
		}


		//-----WYPISANIE WYNIKOW----------
		//jak Ci wygodniej zrob z tego oddzielna funkcje wypisz

		//tworze ta tablice zeby zapisac ktore przedmioty wlozylem do plecaka, 
		//nie moge tego zrobic podczas obliczania bo wynik ciagle sie zmienia
		// nie moge tego zrobic tez podczas sprawdzania czy wlozylem element do plecaka bo sprawdzamy od najwiekszego indeksu
		//a ocet chce miec wypisanie od najmniejszego
		bool *wlozone = new bool[N]();
		// w przypadku bool wartosci poprzez konstruktor () sa ustawiane na false



		//tutaj zmniejszam N i pojemnosc o 1 bo zauwaz ze petle robie ze znakiem < a nie <= wiec np jesli N jest 11 to ostatni indeks jaki przejrze w tablicy to 10 
		//przy wpisaniu musimy zaczac od 10 bo 11 elementu nie ma w tablicy wiec zmniejszam ta wartosc
		int aktualny_n = N - 1,
			aktualny_w = pojemnosc - 1;

		//----- usun komentarz jesli chcesz zobaczyc jak wyglada tablica z wynikami po obliczeniu wszystkich wartosci,
		//jak nie chcesz usun to :) 
		//for (int i = 1; i < N; ++i)					// i - aktualny przedmiot
		//{
		//	for (int j = 1; j < pojemnosc; ++j)		// j - aktualna maxymalna waga
		//	{
		//		cout << K[i][j] << "\t";
		//	}
		//	cout << endl;
		//}
		////******************


		while (aktualny_n > 0) {
			//ten COUT wypisze miejsce w tablicy ktore jest sprawdzane
			//cout << "aktn: " << aktualny_n << ", aktw: " << aktualny_w << endl;
			if (K[aktualny_n][aktualny_w] == K[aktualny_n - 1][aktualny_w])
				aktualny_n = aktualny_n - 1;		//jesli w tablicy element nad rozpatrywanym jest taki sam to znaczy ze tego przedmiotu nie wlozylismy
													//wiec trzeba przeszukiwac dalej
			else
			{
				wlozone[aktualny_n] = true;					//oznaczasz przedmiot ktory wlozyles w tablicy booli
				aktualny_w = aktualny_w - W[aktualny_n];	//przechodzisz do nastepnego elementu 
				aktualny_n = aktualny_n - 1;
			}
		}
		//-------------WYPISANIE------------

		cout << K[N - 1][pojemnosc - 1] << endl;

		for (int i = 0; i < N; ++i) {
			if (wlozone[i] == true)
				cout << i << " ";
		}
		cout << endl;

		//zwolnienie pamieci dynamicznych tablic
		delete[]wlozone;
		for (int i = 0; i < N; ++i)
			delete[]K[i];
		delete[]K;

		return;
	}

	int main()
	{
		int N,				//ilosc przedmiotow
			pojemnosc;		//maxymalny pojemnosc plecaka

							//-------wczytywanie danych-------
		cin >> pojemnosc;
		cin >> N;
		N++;				//zwiekszamy maxymalna pojemnosc i ilosc, ze wzgledu na to ze w tablicy bedziemy operowac na indeksach od 1 do 10 a nie od 0 do 9 
		pojemnosc++;		//tak samo jest na slajdach octa
							//--------------------------------

		int *W = new int[N]();		//tablica z wagami przedmiotow
		int *C = new int[N]();		//tablica z cena (wartoscia) przedmiotow
									//pamietaj ze jezeli przy definiowaniu tablicy dodajesz () to w przypadku zmiennych typu int bedzie ona od razu wypelniona zerami
									//jest to wywolanie konstruktora ktory od razu przy alokacji pamieci wypelnia to zerami

		for (int i = 1; i < N; ++i) {
			cin >> W[i] >> C[i];
		}
		pakowanie_plecaka(W, C, pojemnosc, N);

		//zwolnienie pamieci dynamicznych tablic
		delete[]W;
		delete[]C;
		//system("pause");
		return 0;
	}
}