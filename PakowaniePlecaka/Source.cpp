#include <iostream>
using namespace std;

struct przedmioty
{
	int wartosc;
	float waga;
};
void wypisz(przedmioty *t_przedmiotow, int ilosc)
{
	for (int i = 0; i<ilosc; ++i)
	{
		cout << i << " przedmiot:";
		cout << "\t waga " << t_przedmiotow[i].waga;
		cout << "\t wartosc " << t_przedmiotow[i].wartosc << endl;
	}
}
void wypisz(int **t, int kol, int wier)
{
	for (int j = 0; j<wier; ++j)
	{
		for (int i = 0; i<kol; ++i)
		{
			cout << "\t" << t[i][j];
		}
		cout << endl;
	}
}

int znajdz_ilosc_kolumn(float a, float b)
{
	if (a>b)
		return a * 10;
	else
		return b * 10;
}

void test(int **t, int kol, int wier)
{
	for (int i = 0; i<kol; ++i)
	{
		for (int j = 0; j<wier; ++j)
		{
			t[i][j] = 0;
		}
	}
}

void algorytm(przedmioty *t_przedmiotow, int **t_wartosci, int **t_ilosci, int kolumny, int wiersze)
{
	float dzielnik;

	for (int j = 0; j<wiersze; ++j)
	{
		dzielnik = 0;
		for (int i = 0; i<kolumny; ++i)
		{
			if (j == 0)
			{
				if (t_przedmiotow[j].waga <= dzielnik / 10)
				{
					t_wartosci[i][j] = t_przedmiotow[j].wartosc;
					t_ilosci[i][j] = j + 1;
				}
				else
				{
					t_wartosci[i][j] = 0;
					t_ilosci[i][j] = 0;
				}
			}
			else
			{
				if (t_wartosci[i][j - 1] == 0)                       //jezeli wyzej jest zero 
				{
					if (t_przedmiotow[j].waga <= dzielnik / 10)
					{
						t_wartosci[i][j] = t_przedmiotow[j].wartosc;
						t_ilosci[i][j] = j;
					}
				}
				else if (t_przedmiotow[t_ilosci[i][j - 1] - 1].waga + t_przedmiotow[j].waga <= dzielnik / 10 && t_przedmiotow[t_ilosci[i][j - 1] - 1].wartosc + t_przedmiotow[j].wartosc > t_wartosci[i][j - 1]) // jezeli zmiesci sie suma i ta suma jest mniejsza od wyniku wyzej
				{
					cout << t_przedmiotow[t_ilosci[i][j - 1] - 1].waga + t_przedmiotow[j].waga << " <= " << dzielnik / 10 << endl;
					t_wartosci[i][j] = t_przedmiotow[t_ilosci[i][j - 1] - 1].wartosc + t_przedmiotow[j].wartosc;
					t_ilosci[i][j] = j + 1;
				}
				else
				{
					if (t_przedmiotow[j].waga <= dzielnik / 10 && t_przedmiotow[j].wartosc > t_wartosci[i][j - 1])// && jest wiekszy )
					{
						t_wartosci[i][j] = t_przedmiotow[j].wartosc;
						t_ilosci[i][j] = j + 1;
					}
					else
					{
						cout << t_przedmiotow[t_ilosci[i][j - 1] - 1].waga << " + " << t_przedmiotow[j].waga << " < " << dzielnik / 10 << endl;
						t_wartosci[i][j] = t_wartosci[i][j - 1];
						t_ilosci[i][j] = t_ilosci[i][j - 1];
					}
				}

			}
			dzielnik += 1;
		}
	}
	wypisz(t_wartosci, kolumny, wiersze);
	wypisz(t_ilosci, kolumny, wiersze);

}
int main()
{
	//********************** wczytywanie danych *************************
	int N;
	float pojemnosc, plecak_bartka;

	cin >> pojemnosc >> plecak_bartka;
	cin >> N;

	przedmioty *t_przedmiotow = new przedmioty[N];

	for (int i = 0; i<N; ++i)
	{
		cin >> t_przedmiotow[i].waga >> t_przedmiotow[i].wartosc;
	}
	int kolumny = znajdz_ilosc_kolumn(pojemnosc, plecak_bartka) + 1; // +1 bo musze miec np 0.8
	int **t_wartosci = new int*[kolumny];
	for (int i = 0; i< kolumny; ++i)
		t_wartosci[i] = new int[N];
	int **t_ilosci = new int*[kolumny];
	for (int i = 0; i< kolumny; ++i)
		t_ilosci[i] = new int[N];

	test(t_wartosci, kolumny, N);
	test(t_ilosci, kolumny, N);

	algorytm(t_przedmiotow, t_wartosci, t_ilosci, kolumny, N);

	char znak;
	cin >> znak;
	return 0;
}